<?php

use PHPMailer\PHPMailer\PHPMailer;

require 'phpmailer/src/PHPMailer.php';
require 'phpmailer/src/SMTP.php';

$toemails = array();

$toemails[] = array(
				'email' => 'info@jumalutech.co.za', // Your Email Address
				'name' => 'Jumalu - Website Enquiry' // Your Name
			);

// Form Processing Messages
$message_success = 'We have <strong>successfully</strong> received your Message and will get Back to you as soon as possible.';

// Add this only if you use reCaptcha with your Contact Forms
$recaptcha_secret = '6Lf775MUAAAAAAIYd7kuoRJAwpFqXHmnTQ27kAyn'; // Your reCaptcha Secret

$mail = new PHPMailer();

// If you intend you use SMTP, add your SMTP Code after this Line

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$_POST = json_decode(file_get_contents("php://input"), true);
	// echo $_POST;
	if (!empty($_POST['emailclient2'])) {
		echo '{ "alert": "error", "message": "Error, page not found" }';
	}	elseif($_POST['emailclient1'] != '' ) {
		$name = isset( $_POST['name'] ) ? $_POST['name'] : '';
		$email = isset( $_POST['emailclient1'] ) ? $_POST['emailclient1'] : '';
		$phone = isset( $_POST['phone'] ) ? $_POST['phone'] : '';
		$service = isset( $_POST['service'] ) ? $_POST['service'] : '';
		$coating = isset( $_POST['coating'] ) ? $_POST['coating'] : '';
		$application = isset( $_POST['application'] ) ? $_POST['application'] : '';
		$length = isset( $_POST['fencelength'] ) ? $_POST['fencelength'] : '';
		$location = isset( $_POST['location'] ) ? $_POST['location'] : '';
		$message = isset( $_POST['message'] ) ? $_POST['message'] : '';
		$subject = 'Jumalu - Website Enquiry Contact Page';

			$mail->SetFrom( $email , $name );
			$mail->AddReplyTo( $email , $name );
			foreach( $toemails as $toemail ) {
				$mail->AddAddress( $toemail['email'] , $toemail['name'] );
			}
			$mail->Subject = $subject;

			$name = isset($name) ? "NAME: $name<br><br>" : '';
			$email = isset($email) ? "EMAIL: $email<br><br>" : '';
			$phone = isset($phone) ? "PHONE: $phone<br><br>" : '';
			$service = isset($service) ? "PRODUCT: $service<br><br>" : '';
			$coating = isset($coating) ? "COATING: $coating<br><br>" : '';
			$application = isset($application) ? "APPLICATION: $application<br><br>" : '';
			$length = isset($length) ? "LENGTH: $length<br><br>" : '';
			$location = isset($location) ? "LOCATION: $location<br><br>" : '';
			$message = isset($message) ? "MESSAGE: $message<br><br>" : '';

			$referrer = $_SERVER['HTTP_REFERER'] ? '<br><br><br>This Form was submitted from: ' . $_SERVER['HTTP_REFERER'] : '';

			$body = "$name $email $phone $service $coating $application $length $location $message $referrer";

			// Runs only when File Field is present in the Contact Form
			if ( isset( $_FILES['template-contactform-file'] ) && $_FILES['template-contactform-file']['error'] == UPLOAD_ERR_OK ) {
				$mail->IsHTML(true);
				$mail->AddAttachment( $_FILES['template-contactform-file']['tmp_name'], $_FILES['template-contactform-file']['name'] );
			}
			// FOR TESTING
				// http_response_code(200);
				// header('Content-type: application/json');
				// header('Status: 200 OK');
				// echo json_encode(array(
				//   'statusCode' => 200, // success or not?
				//   'message' => $body
				// 	));
					
			// Runs only when reCaptcha is present in the Contact Form
			if( isset( $_POST['recaptchaToken'] ) ) {

				$recaptcha_data = array(
					'secret' => $recaptcha_secret,
					'response' => $_POST['recaptchaToken']
				);

				$recap_verify = curl_init();
				curl_setopt( $recap_verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify" );
				curl_setopt( $recap_verify, CURLOPT_POST, true );
				curl_setopt( $recap_verify, CURLOPT_POSTFIELDS, http_build_query( $recaptcha_data ) );
				curl_setopt( $recap_verify, CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $recap_verify, CURLOPT_RETURNTRANSFER, true );
				$recap_response = curl_exec( $recap_verify );

				$g_response = json_decode( $recap_response );

				if ( $g_response->success !== true ) {
					echo '{ "alert": "error", "message": "Captcha not Validated! Please Try Again." }';
					die;
				}
			}
			$mail->MsgHTML( $body );
			$sendEmail = $mail->Send();
			if( $sendEmail == true ):
				http_response_code(200);
				header('Content-type: application/json');
				header('Status: 200 OK');
				echo json_encode(array(
				  'statusCode' => 200,
				  'message' => $body
					));
			else:
				echo '{ "alert": "error", "message": "Email <strong>could not</strong> be sent due to some Unexpected Error. Please Try Again later.<br /><br /><strong>Reason:</strong><br />' . $mail->ErrorInfo . '" }';
			endif;
	} else {
		echo '{ "alert": "error", "message": "$body" }';
	}
} else {
	echo '{ "alert": "error", "message": "An <strong>unexpected error</strong> occured. Please Try Again later." }';
}

?>