const commonKeywords = [
]

export const state = () => ({
  client: {
    '@id': `${process.env.baseUrl}`,
    '@type': 'Organization',
    geo: {
      '@type': 'GeoCoordinates',
      latitude: -26.239354,
      longitude: 28.161962
    },
    title:
      'ClamberPrufe Clear View Fencing Gauteng South Africa',
    description:
      'ClamberPrufe Clear View Fencing Specialists located in Germiston, Gauteng South Africa.',
    name: 'ClamberPrufe Clearview Fencing',
    legalName: 'ClamberPrufe Clearview Fencing',
    category: ['Fencing specialists'],
    email: 'info@jumalutech.co.za',
    url: process.env.baseUrl,
    image: [
      `${process.env.baseUrl}/images/slider/clearview-fencing-south-africa.png`,
      `${process.env.baseUrl}/images/slider/high-security-fencing.png`,
      `${process.env.baseUrl}/images/slider/mesh-panel-fencing.png`
    ],
    logo: `${process.env.baseUrl}/jumalutech.png`,
    foundingDate: '2012',
    founders: [
      {
        '@type': 'Person',
        name: 'Jacques Pretorius',
        jobTitle: 'Founder',
        description: '',
        image: '/images/jaline-vandyk.jpg',
        sameAs: [
          'https://www.linkedin.com/in/jaline-pietroiusti-ab354b56'
        ]
      }
    ],
    phone: ['0861100224', '+27 010 216 9205'],
    address: {
      '@type': 'PostalAddress',
      streetAddress: '23 Dakota Crescent, Airport Park, Corner of Dakota and Blackreef Roads',
      addressLocality: 'Germiston',
      addressRegion: 'Gauteng',
      postalCode: '2000',
      addressCountry: 'South Africa'
    },
    contactPoint: {
      '@type': 'ContactPoint',
      contactType: 'Support',
      telephone: '0861100224',
      email: 'info@jumalutech.co.za'
    },
    telephone: '0861100224',
    openingHoursSpecification: [
      {
        '@type': 'OpeningHoursSpecification',
        dayOfWeek: [
          'Monday',
          'Tuesday',
          'Wednesday',
          'Thursday',
          'Friday'
        ],
        opens: '8:00',
        closes: '16:30'
      }
    ],
    sameAs: [
      'https://g.page/theoddwave/',
      'https://www.facebook.com/theoddwave',
      'https://www.instagram.com/theoddwavenz/',
      'https://www.linkedin.com/company/the-odd-wave-ltd/'
    ],
    priceRange: '$$',
    copyright: '© 2020 Copyright The Odd Wave',
    fb: 'https://www.facebook.com/theoddwave',
    instagram: 'https://www.instagram.com/theoddwavenz/',
    linkedin: 'https://www.linkedin.com/company/the-odd-wave-ltd/',
    whatsapp: 'https://chat.whatsapp.com/DT09gv1r4fR7iD05jl7ing',
    googlebusiness: 'https://g.page/theoddwave/review?gm',
    keywords: [
      ...commonKeywords
    ]
  }
})

export const getters = {
  getClient: (state) => {
    const client = state.client
    return client
  },
  getClientFeatures: (state) => {
    return state.client.features
  }
}
