require('dotenv').config()

const org = process.env.ORG || 'jumalutech'
const googleMapApiKey = process.env.GOOGLE_MAP_API_KEY || 'AIzaSyAjh496naiDgsqgBLSsOBBOw4pqb5WF26Y'
const googleTagManagerKey = process.env.GOOGLE_TAG_KEY || 'G-N0XLBDPPDM'
const googleAnalyticsKey = process.env.GOOGLE_ANALYTICS_KEY || 'UA-36271031-1'
const nodeEnv = process.env.NODE_ENV || 'development'
const baseUrl = process.env.BASE_URL || 'http://localhost:3000'
const recaptchaSiteKey = process.env.SITE_KEY || '6Lf775MUAAAAAIRFdC1YeRDzavYfBNuWDVWCLFcD'
// const recaptchaSiteKey = process.env.SITE_KEY || '6LdPAecUAAAAAC904XnGvW88sF_aAxAY6lg1FMUe'
const author = process.env.AUTHOR || 'https://theoddwave.co.nz/'

const routerBase = process.env.DEPLOY_ENV === 'GH_PAGES' ? {
  router: {
    base: `/${org}/`
  }
} : {}

module.exports = {
  env: {
    googleMapApiKey,
    baseUrl,
    recaptchaSiteKey,
    author,
    nodeEnv
  },
  mode: 'universal',
  render: {
    static: {
      setHeaders(res) {
        res.setHeader("Set-Cookie", "HttpOnly;Secure;SameSite=Strict")
      }
    }
  },
  // pageTransition: {
  //   name: 'page',
  //   mode: 'out-in'
  // },
  generate: {
    // subFolders: false,
    fallback: true
  },
  ...routerBase,
  head: {
    htmlAttrs: {
      lang: 'en'
    },
    title: process.env.npm_package_name.toUpperCase() + ' - Clamberprufe Clear View Fencing Gauteng South Africa',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'robots', name: 'robots', content: 'index, follow' },
      { hid: 'author', name: 'author', content: author },
      { 'http-equiv': 'X-UA-Compatible', content: 'IE=edge' },
      { 'http-equiv': 'content-type', content: 'text/html; charset=UTF-8' },
      { 'http-equiv': 'Cache-Control', content: 'no-cache, no-store, must-revalidate' },
      { 'http-equiv': 'Pragma', content: 'no-cache' },
      { 'http-equiv': 'Expires', content: '0' },
      { hid: 'og:type', property: 'og:type', content: 'website' },
      { hid: 'og:site_name', property: 'og:site_name', content: 'Jumalutech' },
      { hid: 'og:image', property: 'og:image', content: `${baseUrl}/jumalutech.jpg` },
      { hid: 'og:url', property: 'og:url', content: baseUrl },
      {
        hid: 'og:description',
        name: 'og:description',
        content: 'ClamberPrufe Clear View Fencing Specialists located in Germiston, Gauteng South Africa.'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'ClamberPrufe Clear View Fencing Specialists located in Germiston, Gauteng South Africa.'
      },
      {
        hid: 'copyright',
        name: 'copyright',
        content: 'ClamberPrufe Clearview - Jumalu Fencing'
      },
      { mobileAppIOS: true },
      { name: 'apple-mobile-web-app-capable', content: 'yes' },
      { name: 'apple-mobile-web-app-title', content: 'ClamberPrufe' },
      { name: 'apple-mobile-web-app-status-bar-style', content: 'black' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', href: '/apple-icon-152x152.png' },
      { rel: 'author', href: `${author}` },
      { rel: 'apple-touch-icon', sizes: "57x57", type: 'image/x-icon', href: '/images/favicons/apple-icon-57x57.png' },
      { rel: 'apple-touch-icon', sizes: "60x60", type: 'image/x-icon', href: '/images/favicons/apple-icon-60x60.png' },
      { rel: 'apple-touch-icon', sizes: "152x152", type: 'image/x-icon', href: '/images/favicons/apple-icon-152x152.png' },
      { rel: 'apple-touch-icon', sizes: "167x167", type: 'image/x-icon', href: '/images/favicons/apple-icon-167x167.png' },
      { rel: 'apple-touch-icon', sizes: "180x180", type: 'image/x-icon', href: '/images/favicons/apple-icon-180x180.png' },
      { rel: 'mask-icon', href: '/images/favicons/apple.svg', color: 'blue' },
      { rel: "apple-touch-startup-image", href: "/images/splash/iphone5_splash.png", media: "(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" },
      { rel: "apple-touch-startup-image", href: "/images/splash/iphone6_splash.png", media: "(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" },
      { rel: "apple-touch-startup-image", href: "/images/splash/iphoneplus_splash.png", media: "(device-width: 621px) and (device-height: 1104px) and (-webkit-device-pixel-ratio: 3)" },
      { rel: "apple-touch-startup-image", href: "/images/splash/iphonex_splash.png", media: "(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)" },
      { rel: "apple-touch-startup-image", href: "/images/splash/iphonexr_splash.png", media: "(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)" },
      { rel: "apple-touch-startup-image", href: "/images/splash/iphonexsmax_splash.png", media: "(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)" },
      { rel: "apple-touch-startup-image", href: "/images/splash/ipad_splash.png", media: "(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)" },
      { rel: "apple-touch-startup-image", href: "/images/splash/ipadpro1_splash.png", media: "(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)" },
      { rel: "apple-touch-startup-image", href: "/images/splash/ipadpro3_splash.png", media: "(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2)" },
      { rel: "apple-touch-startup-image", href: "/images/splash/ipadpro2_splash.png", media: "(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)" },
      // { rel: "apple-touch-startup-image", href: "/images/splash/launch-1125x2436.png", media: "(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)" },
      // { rel: "apple-touch-startup-image", href: "/images/splash/launch-750x1334.png", media: "(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)" },
      // { rel: "apple-touch-startup-image", href: "/images/splash/launch-1242x2208.png", media: "(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)" },
      // { rel: "apple-touch-startup-image", href: "/images/splash/launch-640x1136.png", media: "(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)" }
    ],
    script: [
      { src: '/clickcease.js',  body: true },
    ],
    noscript: [{ innerHTML: "<a href='https://www.clickcease.com' rel='nofollow'><img src='https://monitor.clickcease.com/stats/stats.aspx'  alt='ClickCease'/></a>" , type: 'text/javascript', body: true }],
    __dangerouslyDisableSanitizers: ['noscript']
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [
    // '@/assets/custom.scss',
    '@/assets/bootstrap.css',
    '@/assets/main.css',
    '@/assets/dark.css',
    '@/assets/font-icons.css',
    '@/assets/animate.css',
    '@/assets/magnific-popup.css',
    '@/assets/clearview/fonts.css',
    '@/assets/clearview/colors.css',
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/vue-awesome-swiper', mode: 'client' },
    { src: '~/plugins/google-maps', mode: 'client' },
    { src: '~/plugins/aos', mode: 'client' },
    { src: '~/plugins/vue-countup-v2', mode: 'client' },
    { src: '~/plugins/vue-notifications', mode: 'client' }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/gtm',
    '@nuxtjs/google-analytics'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    'nuxt-responsive-loader',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    '@nuxtjs/robots',
    '@nuxtjs/sitemap',
    'nuxt-webfontloader',
    '@nuxtjs/recaptcha',
  ], 
  recaptcha: {
    siteKey: recaptchaSiteKey,
    version: 2
  },
  webfontloader: {
    custom: {
      families: [
        'Source+Sans+Pro:300,400,600,700',
        'Roboto:300,400,500,700'
      ],
      urls: [
        'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Roboto:300,400,500,700',
      ]
    },
    timeout: 2000
  },
  bootstrapVue: {
    bootstrapCSS: false,
    bootstrapVueCSS: false
    // components: ['BContainer', 'BRow', 'BCol', 'BFormInput', 'BButton', 'BTable', 'BModal', 'BNavBar', 'BNav'],
    // directives: ['VBModal', 'VBPopover', 'VBTooltip', 'VBScrollspy']
  },
  responsiveLoader: {
    name: `img/${org}-[name]-[hash:7]-[width].[ext]`,
    quality: 70,
    sizes: [375, 425, 768, 1024, 1440],
    adapter: require('responsive-loader/sharp'),
    placeholder: true
  },
  robots: {
    UserAgent: '*',
    // Disallow: [
    //   '/products/pro-business-website-landing/'
    // ],
    Sitemap: baseUrl + '/sitemap.xml'
  },
  sitemap: {
    hostname: baseUrl,
    gzip: false,
    xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9',
    trailingSlash: true,
    defaults: {
      changefreq: 'daily',
      lastmod: new Date(),
      lastmodrealtime: true
    }
  },
  recaptcha: {
    siteKey: recaptchaSiteKey,
    version: 2
  },
  googleAnalytics: {
    id: googleAnalyticsKey
  },
  gtm: {
    id: googleTagManagerKey
  },
  pwa: {
    manifest: {
      name: 'Jumalutech ClamberPrufe',
      short_name: 'ClamberPrufe',
      description: 'ClamberPrufe Clear View Fencing Specialists',
      lang: 'en',
      display: 'standalone',
    },
    /*
    ** Handle external assets
    */
    workbox: {
      runtimeCaching: [
        {
          urlPattern: 'https://fonts.googleapis.com/.*',
          handler: 'cacheFirst',
          method: 'GET',
          strategyOptions: { cacheableResponse: { statuses: [0, 200] } }
        }
      ]
    }
  },
  /*
  ** Allow dev tools in production
  */
  vue: {
    config: {
      productionTip: false,
      devtools: true
    }
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      config.module.rules.push({
        enforce: 'pre',
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        exclude: /(node_modules)/,
        options: {
          fix: true
        }
      })
    }
  }
}
