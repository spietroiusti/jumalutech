# PROJECT SETUP

## npm install @nuxtjs/sitemap --save

## npm install @nuxtjs/robots --save

## npm install @nuxtjs/gtm --save

Ref: https://github.com/nuxt-community/gtm-module

Add @nuxtjs/gtm to the buildModules section of nuxt.config.js
{
  buildModules: [
    '@nuxtjs/gtm',
  ],
  gtm: {
    id: 'GTM-XXXXXXX'
  }
}

## npm install @nuxtjs/google-analytics --save

Ref: https://www.npmjs.com/package/@nuxtjs/google-analytics

 buildModules: [
    ['@nuxtjs/google-analytics', {
      id: 'UA-12301-2'
    }]
  ]

## npm install nuxt-responsive-loader 

  responsiveLoader: {
    name: `img/${org}-[hash:7]-[width].[ext]`,
    quality: 70,
    // min: 320,
    // max: 1440,
    // steps: 6,
    sizes: [320, 375, 425, 768, 1024, 1440],
    // format: 'png',
    adapter: require('responsive-loader/sharp'),
    placeholder: true
  }